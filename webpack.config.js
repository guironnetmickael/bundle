const path = require("path");

const config = {
  mode: "development",
  // Point d'entrée de Webpack
  entry: {
    myApp: ["./src/main.ts"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/, // pour les fichiers se terminant pas ts ou tsx
        exclude: /node_modules/, // sans prendre en compte les dépendances
        loader: 'babel-loader',
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
};

module.exports = config;

// pnpm webpack build
// node dist/bundle.js

// https://coopernet.fr/formation/js/webpack
// https://dev.to/sukanto113/how-to-configure-typescript-with-webpack-babel-and-mocha-2433
// https://blogs.infinitesquare.com/posts/web/les-loaders-webpack-l-exemple-avec-sass